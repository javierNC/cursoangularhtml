import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductoInterface } from '../interfaces/producto.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  cargando = true;
  producto: ProductoInterface[] = [];
  productoFiltrado: ProductoInterface[ ] = [];

  constructor( private http: HttpClient ) {

    this.cargarProductos();

  }

  private cargarProductos(){
    // las promesas tienen un callback que reciben 2 argumentos
    return new Promise((resolve, reject) => {

      this.http.get('https://angularhtml-988c5.firebaseio.com/productos_idx.json')
      .subscribe( (resp: ProductoInterface[]) => {
        this.producto = resp;
        this.cargando = false;
        resolve();
      });

    });

  }

  getProducto( productoId: string ){
    return this.http.get(`https://angularhtml-988c5.firebaseio.com/productos/${ productoId }.json`);
  }

  buscarProducto(termino: string){
    if( this.producto.length === 0){
      // cargar productos
      this.cargarProductos().then( () => {
        // Aplicar filtro despues de tener los productos
        this.filtrarProductos(termino);
      });
    }else{
      // aplica filtro
      this.filtrarProductos(termino);
    }
  }

  private filtrarProductos( termino ){
    console.log(this.producto);
    this.productoFiltrado = [];

    termino = termino.toLocaleLowerCase();

    this.producto.forEach( prod => {

      const tituloLower = prod.titulo.toLocaleLowerCase();

      if ( prod.categoria.indexOf( termino ) >= 0  || tituloLower.indexOf( termino ) >= 0){
        this.productoFiltrado.push( prod );
      }
    });
  }
}
