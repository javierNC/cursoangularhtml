import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductosService } from '../../services/productos.service';
import { ProductoDescripcion } from '../../interfaces/producto-descripcion.interface';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  producto: ProductoDescripcion;
  productoId: string;

  constructor( private route: ActivatedRoute, public productoService: ProductosService ) { }

  ngOnInit(): void {
    this.route.params
      .subscribe( parametros => {
       // console.log(parametros['productoId']);
        this.productoService.getProducto(parametros['productoId'])
          .subscribe( (producto: ProductoDescripcion) => {
            this.productoId = parametros['productoId'];
            this.producto = producto;
          });

      });
  }

}
