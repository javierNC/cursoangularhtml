export interface InfoPagina{
    titulo?: string;
    nombre_corto?: string;
    facebook?: string;
    twitter?: string;
    equipo_trabajo?: any[];
  }